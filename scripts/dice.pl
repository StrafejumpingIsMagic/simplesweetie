#!/usr/bin/perl -X
if ($ARGV[1] eq 'PRIVMSG') {
if ($ARGV[2] =~ m/#./) {
	my $o = '';
	my $a = '';
	if ($ARGV[3] =~ m/(sweetie|sweetie,|sweetie:|sweetiebot|sweetiebot,|sweetiebot:) (please roll|would you roll|roll) (a|the|another) ./i) {
		$o = 'roll'; $a = $ARGV[3];
		$a =~ s/(sweetie|sweetie,|sweetie:|sweetiebot|sweetiebot,|sweetiebot:) (please roll|would you roll|roll) (a|the|another) //i;
	} elsif ($ARGV[3] =~ m/(sweetie|sweetie,|sweetie:|sweetiebot|sweetiebot,|sweetiebot:) (please roll|would you roll|roll) ./i) {
		$o = 'roll'; $a = $ARGV[3];
		$a =~ s/(sweetie|sweetie,|sweetie:|sweetiebot|sweetiebot,|sweetiebot:) (please roll|would you roll|roll) //i;
	} elsif ($ARGV[3] =~ m/(someone roll|please roll|would you roll|roll) (a|the|another) ./i) {
		$o = 'roll'; $a = $ARGV[3];
		$a =~ s/(someone roll|please roll|would you roll|roll) (a|the|another) //i;
	} elsif ($ARGV[3] =~ m/(someone roll|roll) ./i) {
		$o = 'roll'; $a = $ARGV[3];
		$a =~ s/(someone roll|roll) //i;
	}

	if ($o eq 'roll') {
		#print "PRIVMSG " . $ARGV[2] . " :\$a = $a;\n";
		if ($a =~ m/.d./i) {
			my $sp = $a; $sp =~ s/\ \ /\ /g;
			my @darg = split(/ /,$sp);
			my $ao = $a;
			$a = $darg[0];
			my $count = $a; $count =~ s/d.*$//i;
			my $size = $a; $size =~ s/^.*d//i;
			$a = $ao;
			if ($count lt 1) {
				print "PRIVMSG " . $ARGV[2] . " :What kind of dice is $a?\n";
			} else {
				print "PRIVMSG " . $ARGV[2] . " :\x01ACTION picks up $count d$size dice with her teeth and throws them on the table.\x01\n";
				my @r;
				for my $i ( 1..$count ) {
					push(@r,( int(1 + rand($size)) ));
				}
					print "PRIVMSG " . $ARGV[2] . " :";
				if ($count gt 1) {
					my $tstr = '';
					for my $i (0..($#r - 1)) {
						$tstr .= $r[$i] . ', ';
					}
					chop($tstr); chop($tstr);
					print $tstr . " and " . $r[$#r];
				} else {
					print $r[0];
				}
				if (exists $darg[1] and defined($darg[1])) {
					my $extra = $a; $a =~ s/^.*d*\ //i;
					my $do_sum = 0;
					if ($extra =~ m/damage/i) {
						$do_sum = 1;
					}
					if ($do_sum) {
						my $sum = 0;
						foreach (@r) {
							$sum += $_;
						}
						print " for $sum total";
					}
				}
				print ".\n";
			}
		} else {
			print "PRIVMSG " . $ARGV[2] . " :What kind of dice is $a?\n";
		}
	}
}
}
