#include <iostream>
#include <string>
#include <boost/asio.hpp>
#include <boost/filesystem.hpp>
#include <unistd.h>
#include <config.h>

#ifdef DEBUG
	#define debug(...) __VA_ARGS__
#else
	#define debug(...) ;
#endif

using boost::asio::ip::tcp;
using namespace boost::filesystem;

using namespace std;

#define READ_BUFFER_SIZE 1024
#define PIPE_READ_BUFFER_SIZE 1024

const int state_exiting = 0;
const int state_running = 1;

int state = state_running;

string cline = "";
string output = "";

void procLine() {
	if (cline.size() == 0) return;
	vector<string> tok;
	size_t cur;
	size_t next = -1;
	do {
		cur = next + 1;
		next = cline.find_first_of(" ", cur);
		size_t br = cline.find_first_of(":", cur);
		if (br == string::npos || br > next || cur == 0) {
			tok.push_back(cline.substr(cur, next - cur));
		} else if (cline.at(cur) == 58) {
			tok.push_back(cline.substr(cur + 1, string::npos));
			next = string::npos;
		}
	} while (next != string::npos);
	debug(
		for (unsigned int i = 0; i < tok.size(); i++)
			cout << "tok[" << to_string(i) << "] = (" << tok[i] << ");\n\n";
	);
	cline = "";

	// ping response
	if (tok[0].compare("PING") == 0 && tok.size() == 2) {
		output += string("PONG :") + tok[1] + string("\n");
	}
	// logged in
	if (tok.size() > 1 && tok[0].at(0) == 58 && tok[1].compare("001") == 0) {
		if (chan.length() > 0) {
			output += string("JOIN ") + chan + string("\n");
			output += string("PRIVMSG ") + chan + string(" :am i alive? :o\n");
		}
	}
	// call scripts and or binaries in modules path
	string mod_args = "";
	for (unsigned int i = 0; i < tok.size(); i++) {
		mod_args += "'";
		for (unsigned int n = 0; n < tok[i].length(); n++) {
			char c =  tok[i].at(n);
			if (c == 39)
				mod_args += "'\\''";
			else
				mod_args += c;
		}
		mod_args += "' ";
	}
	if ( exists(scriptpath) ) {
		debug(cout << "module dir exists\n");
		directory_iterator end_iter;
		for (directory_iterator iter(scriptpath); iter != end_iter; ++iter) {
			if (is_directory(iter->status())) {
				// could do with a separate function for recursion
			} else {
				string mod = iter->path().string();
				string cmd = mod + string(" ") + mod_args;

				debug(cout << cmd << endl);
				// open a pipe to use for receiving commands to send
				FILE* pipe = popen(cmd.c_str(), "r");
				if (pipe) {
					char c[PIPE_READ_BUFFER_SIZE];
					string r = "";
					debug(cout << "Waiting on script...\n");
					while (!feof(pipe)) {
						if (fgets(&c[0], PIPE_READ_BUFFER_SIZE, pipe) != NULL) {
							cout << "Got something.";
							r += c;
						} else {
							usleep(10000);
						}
					}
					pclose(pipe);
					debug(cout << "Script done.\n");
					/* don't mess up new lines, scripts should only
					 * output complete commands but just in case they
					 * don't this check is here. */
					if (r.length() > 0 && r.at(r.length() - 1) == 10)
						output += r;
				}
			}
		}
	}
}

int main(int argc, char* argv[]) {
	boost::asio::io_service ios;

	tcp::resolver resolver(ios);
	tcp::resolver::query query(serv, port);
	tcp::resolver::iterator ep_iter = resolver.resolve(query);
	tcp::resolver::iterator ep_ed;

	tcp::socket socket(ios);
	boost::system::error_code error = boost::asio::error::host_not_found;
	while (error && ep_iter != ep_ed) {
		socket.close();
		socket.connect(*ep_iter++, error);
	}
	if (error)
		throw boost::system::system_error(error);
	
	vector<char> buf(READ_BUFFER_SIZE);

	if (pass.length() > 0) output += string("PASS ") + pass + string("\n");
	output += string("NICK ") + nick + string("\n");
	output += string("USER ") + user + string(" 4 0 :") + realname + string("\n");

	while (state == state_running) {
		size_t length = socket.read_some(boost::asio::buffer(buf), error);

		if (error == boost::asio::error::eof)
			exit(1);
		else if (error)
			throw boost::system::system_error(error);

		if (length > 0) {
			for (unsigned int i = 0; i < length; i++) {
				debug(cout << buf[i]);
				if (buf[i] == 10 || buf[i] == 13) {
					procLine();
				} else {
					cline += buf[i];
				}
			}
		}

		if (output.length() > 0) {
			boost::asio::write(socket, boost::asio::buffer(output), boost::asio::transfer_all(), error);
			debug(cout << "» " << output);
			output = "";
		}
		usleep(10000);
	}

	return 0;
}
