CXX=g++
ifeq (${BUILD_TYPE}, release)
CFLAGS=-O2 -ffast-math -fomit-frame-pointer -Wall -std=c++11
else ifeq (${BUILD_TYPE}, small)
CFLAGS=-Os -ffast-math -fomit-frame-pointer -Wall -std=c++11
else
CFLAGS=-O0 -g -Wall -Werror -std=c++11 -DDEBUG
endif
INCLUDE_DIRS=-I./ -I./src
SWEETIE_BINARY=sweetie
LIBS=-lboost_system -lboost_filesystem -lpthread

SWEETIE_SRC=\
	src/core.cpp \

${SWEETIE_BINARY}: ${SWEETIE_SRC}
	${CXX} ${CFLAGS} ${INCLUDE_DIRS} ${SWEETIE_SRC} -o ${SWEETIE_BINARY} ${LIBS}
clean:
	rm -v ${SWEETIE_BINARY}
testrun: ${SWEETIE_BINARY}
	${SWEETIE_BINARY}
